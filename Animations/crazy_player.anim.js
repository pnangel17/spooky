Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});

	module.spritesheet['crazy_player'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/CrazyAnimation.png',
		frameWidth : 32,
		frameHeight : 32,
		framesPerSecond : 12
	});


  let anim_id = 'crazy_player';
  
	module.sequence[anim_id] = {};
  
	module.sequence[anim_id]['Standing_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 0,
		endFrame : 0,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Standing_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 0,
		endFrame : 0,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	module.sequence[anim_id]['Walking_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 0,
		endFrame : 3,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Walking_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 0,
		endFrame : 3,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	module.sequence[anim_id]['Jumping_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 2,
		endFrame : 2,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Jumping_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 2,
		endFrame : 2,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	module.sequence[anim_id]['Falling_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 1,
		endFrame : 1,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Falling_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['crazy_player'],
		startFrame : 1,
		endFrame : 1,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));