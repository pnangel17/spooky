'use strict';

// Base class for character / entity types within the game

Game.Model = (function(module) {
  
  module.Entity = function(model = {}) {
    
    let entity = {};
    
    let ps = {
      
      stateGraph : {},
      currentState : null,
      currentAnimationSequence : null
    };
    
    // Private API
    
    let stateGraph = function() {
      
      return ps.stateGraph;
    }
    
    let addState = function(id, state) {
      
      ps.stateGraph[id] = state;
    }
    
    let currentState = function() {
      
      return ps.currentState;
    }
    
    // Set the current state and call the 'enter' method (hence the need for env and tDelta parameters)
    let setCurrentState = function(id, env, tDelta) {
    
      ps.currentState = ps.stateGraph[id];
      
      if (ps.currentState!==null && ps.currentState!==undefined) {
        
        ps.currentState.enter(this, env, tDelta);
      }
    }
    
    let currentAnimationSequence = function() {
      
      return ps.currentAnimationSequence;
    }
    
    let setCurrentAnimationSequence = function(sequence) {
    
      ps.currentAnimationSequence = sequence;
    }
    
    
    // Main update interface - sub-types determine kinematic and dyamic behaviour
    let update = function(env, tDelta) {
      
      if (ps.currentState) {
        
        return ps.currentState.update(this, env, tDelta);
       
      } else {
        
        // By default return true to tell the caller we're 'alive'.  This enables objects to adopt a single state 'existing' model whereby we don't need the overhead of defining state functors for static behaviour.
        return true;
      }
    }
    
    
    let addTransition = function(id, targetID, conditionFunction, actionFunction = null) {
      
      if (id == null) {
        
        // No source state specified so transition to targetID is assigned to ALL existing states.
        for (let state in ps.stateGraph) {
          
          // Ignore inherited properties
          if (ps.stateGraph.hasOwnProperty(state) && ps.stateGraph[state] != null) {
          
            ps.stateGraph[state].addTransition(ps.stateGraph[targetID], conditionFunction, actionFunction);
          }
        }
        
      } else {
      
        // Add targetID transition to specified source state.
        ps.stateGraph[id].addTransition(ps.stateGraph[targetID], conditionFunction, actionFunction);
      }
    }
    
    let processTransitions = function(env, tDelta) {
      
      if (ps.currentState) {
        
        // eval transitions passing host object on which states relate (this)
        let transitionState = ps.currentState.evalTransitions(this, env, tDelta);
      
        if (transitionState.stateChanged) {
          
          // Process exit of current state
          ps.currentState.exit(this, env, tDelta);
          
          // Call the action function if defined for the transition that's occuring
          if (transitionState.transitionAction) {
          
            transitionState.transitionAction(env, tDelta);
          }
          
          // After existing state exit and transition actions have been performed, perform actual state change.  Note: We actually get the state object in transitionState.newState so can set directly and call the 'enter' method.
          ps.currentState = transitionState.newState;
          
          // Once transition is complete, the first thing we do is call the new state's 'enter' method
          ps.currentState.enter(this, env, tDelta);
        }
      }
    };
    
    // Public interface
    
    // Draw entity
    entity.draw = null; // Override in subclass //function(context, canvas) {};
    
    // Update entity - called before any Matter.js updates occur
    entity.update = update;
    
    // Manage entity state transitions - processTransitions is called after Matter.js updates occur
    entity.addTransition = addTransition;
    entity.processTransitions = processTransitions;
    
    entity.stateGraph = stateGraph;
    entity.addState = addState;
    entity.currentState = currentState;
    entity.setCurrentState = setCurrentState;
    entity.currentAnimationSequence = currentAnimationSequence;
    entity.setCurrentAnimationSequence = setCurrentAnimationSequence;
    
    return entity;
  }
  
  
  return module;
  
})((Game.Model || {}));